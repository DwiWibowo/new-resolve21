(function($){
	$('.js-mover').on('click', function(){
		$(this).addClass('show');
		$(this).removeClass('mover');
		$('.js-seconder').addClass('seconder');
	});
  $('.js-seconder').on('click', function(){
		$(this).addClass('show');
		$(this).removeClass('seconder');
		$('.js-reset').addClass('show');
	});
  $('.js-reset').on('click', function(){
		$(this).removeClass('show');
		$('.js-mover').removeClass('show');
		$('.js-mover').addClass('mover');
		$('.js-seconder').removeClass('show');
	});


  $('.js-imoved').on('click', function(){
		$(this).removeClass('btn-primary');
		$(this).addClass('btn-secondary');
		$('.js-isecond').removeClass('btn-secondary');
		$('.js-isecond').addClass('btn-warning');
	});
	$('.js-isecond').on('click', function(){
		$(this).removeClass('btn-warning');
		$(this).addClass('btn-secondary');
		$('.js-imoved').removeClass('btn-secondary');
		$('.js-imoved').addClass('btn-primary');
	});
	
})(jQuery);